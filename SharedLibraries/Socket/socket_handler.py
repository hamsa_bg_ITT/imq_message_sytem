import sys
sys.path.append('../')
import socket
from SharedLibraries.Protocols.protocol_parser import Parser
from SharedLibraries.Protocols.request import Request
from Utilities import constants
from Utilities.crypter import Crypter
from Utilities.Handlers.log_handler import LogHandler
from SharedLibraries.ExceptionHandling.socket_exception import SocketException

logger_obj = LogHandler()
crypter_obj = Crypter

class SocketHandler():

    def __init__(self):
        self.host = socket.gethostname()
        self.port = 8080
        
    def create_socket(self):
        try:
            self.socket_obj = socket.socket()
            return True
        except SocketException:
            self.log_error()
        
    def set_socket(self,socket):
        self.socket_obj = socket
    
    def socket_bind(self):
        try:
            self.socket_obj.bind((self.host, self.port))
            return True
        except SocketException:
            self.log_error()
            
    def socket_listen(self):
        try:
            print('Waiting for a Connection..')
            self.socket_obj.listen(5)
            return True
        except SocketException:
            self.log_error()

    def connect_socket(self):
        try:
            print('Waiting for connection...')
            self.socket_obj.connect((self.host, self.port))
            print('Connected..!!')
            return True
        except SocketException:
            logger_obj.log_error(str(e))
            print(str(e))
            self.socket_obj.close()

    def socket_accept(self):
        try:
            client, address = self.socket_obj.accept()
            return client, address
            return True
        except SocketException:
            self.log_error()
    
    def close_socket(self):
        try:
            self.socket_obj.close()
            print("Connection closed")
            return True
        except SocketException:
            self.log_error()

    def recv_response(self):
        try:
            data = self.socket_obj.recv(2048)
            if not data:
                return False
            parsed_response = Parser.get_json_decoded(data)
            return parsed_response
        except SocketException:
           self.log_error()
            
    def send_response(self, request, type, status = constants.REQUEST_SUCCESS):
        try:
            response_header = Request(request,type,status)
            parsed_header = Parser.get_json_encoded(response_header)
            self.socket_obj.send(str.encode(parsed_header))
            return True
        except SocketException:
            self.log_error()

    def log_error(self):
        logger_obj.log_error(str(socket.error))
        print(str(socket.error))
        self.socket_obj.close()