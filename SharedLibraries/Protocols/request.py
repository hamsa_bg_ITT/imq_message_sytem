from SharedLibraries.Protocols.imq_protocol import ImqProtocol
from dataclasses import dataclass

@dataclass
class Request(ImqProtocol):
    requestType: str = "connect_to_topic"
