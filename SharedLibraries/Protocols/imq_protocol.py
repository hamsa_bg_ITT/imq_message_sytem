from dataclasses import dataclass


@dataclass
class ImqProtocol:
    data: str
    request_type: int
    status: int = 1
    version: int = 5
    format: str = "json"
    
