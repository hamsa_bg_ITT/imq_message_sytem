
from SharedLibraries.Protocols.imq_protocol import ImqProtocol
import jsonpickle
import json

class Parser:
    def get_json_encoded(header):
       return json.dumps(jsonpickle.encode(header, unpicklable=False), indent=4)

    def get_json_decoded(header):
        header = json.loads(header)
        return json.loads(header)
