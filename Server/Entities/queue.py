from Utilities import constants
from Utilities.datetime import Datetime
datetime_obj = Datetime()

queues = {'dead_queue':[]} #intialiasing variable to hold queues
class Queue():
    def __init__(self,database_handler):
        self.database_handler = database_handler

    def create_queue_for_topic(self,topic_name):
        query = 'CREATE TABLE IF NOT EXISTS `'+topic_name+'_queue` (`id` INT AUTO_INCREMENT PRIMARY KEY, `message_id` INT,`subscriber_id` INT NOT NULL,message_status_id INT NOT NULL DEFAULT 1,CONSTRAINT '+topic_name+'_queue_fk_clients FOREIGN KEY (subscriber_id) REFERENCES clients(id), CONSTRAINT '+topic_name+'_queue_fk_status FOREIGN KEY (message_status_id) REFERENCES message_status(id),CONSTRAINT '+topic_name+'_queue_fk_message FOREIGN KEY (message_id) REFERENCES messages(id))'
        self.database_handler.execute_query(query)
        if not queues.__contains__(topic_name):
            queues[topic_name]=[]
        
    def insert_into_topic_queue(self,topic_name,subscriber_id,message_id):
        query = "INSERT into `"+topic_name+"_queue` (`message_id`,`subscriber_id`) values('"+str(message_id)+"','"+str(subscriber_id)+"')"
        self.database_handler.execute_query(query)

    def enqueue(self,topic_name,subscriber_id,message):
        expires_at = datetime_obj.get_next_day()
        message = {'message':message,'subscriber_id':subscriber_id,'message_status':1,'expires_at':expires_at}
        queues[topic_name].insert(len(queues[topic_name]),message)

    def dequeue(self,topic_name,message):
        queues[topic_name].remove(message)

    def get_messages_from_queue(self,topic_name,client_id):
        if(queues.__contains__(topic_name)):
            messages_in_channel = queues[topic_name]
            index = 0
            messages_to_send = ''
            current_date = datetime_obj.get_current_datetime()
            if(len(messages_in_channel)):
                for message in messages_in_channel:
                    if(int(message['subscriber_id'])==int(client_id) and message['message_status']==constants.MESSAGE_STATUS_READY):
                        messages_to_send = messages_to_send+"\n"+message['message']
                        self.update_message_status(message,topic_name,index)
                    index = index+1
                return messages_to_send

        return False

    def update_message_status(self,message,topic_name,index):
        queues[topic_name][index]['message_status'] = constants.MESSAGE_STATUS_DELIVERED 
        expire_date = message['expires_at']
        current_date = datetime_obj.get_current_datetime()
        if(expire_date<current_date):
            self.dequeue(topic_name,message)   
            self.enqueue('dead_queue',client_id,message['message'])   