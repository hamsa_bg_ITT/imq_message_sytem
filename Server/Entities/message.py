from Utilities.datetime import Datetime
datetime_obj = Datetime()

class Message():

    def __init__(self,database_handler):
        self.database_handler = database_handler

    def save_messages(self,topic_id,message,publisher_id,expires_at = None):
        if expires_at is None:
            expires_at = datetime_obj.get_next_day()
        query = "INSERT into `messages` (`message`,`created_by`,`topic_id`,`expires_at`) values('"+message+"','"+str(publisher_id)+"','"+str(topic_id)+"','"+str(expires_at)+"')"
        self.database_handler.execute_query(query)
        return self.database_handler.mycursor._insert_id
    
    def get_messages(self,topic_name,subscriber_id):
        self.update_dead_message_status(topic_name)
        query = 'SELECT `message` FROM `messages` WHERE `id` IN (SELECT `message_id` from `'+str(topic_name)+'_queue` WHERE subscriber_id = '+str(subscriber_id)+' AND message_status_id = (SELECT id from message_status WHERE status = "READY"))'
        return self.database_handler.fetch_query(query)
    
    def update_message_status(self,topic_name,subscriber_id):
        query = 'UPDATE `'+str(topic_name)+'_queue` SET message_status_id = (SELECT id from message_status WHERE status = "DELIVERED") WHERE subscriber_id = '+str(subscriber_id)+' AND message_status_id = (SELECT id from message_status WHERE status = "READY")'
        return self.database_handler.execute_query(query)

    def update_dead_message_status(self,topic_name):
        query = 'UPDATE `'+str(topic_name)+'_queue` SET message_status_id = (SELECT id from message_status WHERE status = "DEAD") WHERE message_id IN (SELECt id from messages WHERE DATE(expires_at)>DATE(NOW()))'
        return self.database_handler.execute_query(query)