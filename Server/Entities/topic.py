

class Topic():
    def __init__(self,database_handler):
        self.database_handler = database_handler

    def get_topics(self):
        query = "SELECT id,topic_name FROM `topics`"
        return self.database_handler.fetch_query(query)
    
    def get_subscribed_topics(self,subscriber_id):
        query = "SELECT id,topic_name from `topics` WHERE id IN(SELECT topic_id from subscribers WHERE `client_id` = '"+str(subscriber_id)+"')"
        return self.database_handler.fetch_query(query)

    def get_topic_id(self,topic_name):
        query = "SELECT id from topics WHERE topic_name = '"+topic_name+"'"
        return self.database_handler.fetchone_query(query)
    
    def add_subscription(self,topic_id,client_id):
        query = "INSERT into `subscribers` (`topic_id`,`client_id`) values('"+str(topic_id)+"','"+str(client_id)+"')"
        self.database_handler.execute_query(query)
        
    def get_subcribers(self,topic_id):
        query = 'SELECT client_id from subscribers WHERE topic_id = '+str(topic_id)
        return self.database_handler.fetch_query(query)

    def insert_into_topics_table(self,topic_name,creater_id):
        query = "INSERT into `topics` (`topic_name`,`created_by`) values('"+topic_name+"','"+str(creater_id)+"')"
        self.database_handler.execute_query(query)