from Server.Entities.queue import Queue

class QueueService():

    def __init__(self,database_handler):
        self.database_handler = database_handler
        self.queue_obj = Queue(database_handler)

    def push_messages(self,subscribers,topic_name,message_id,message):
        if(subscribers):
            for subscriber in subscribers:
                subscriber_id = subscriber[0]
                self.insert_into_topic_queue(topic_name,subscriber_id,message_id)
                self.queue_obj.enqueue(topic_name,subscriber_id,message)
        return True        
    
    def create_queue_for_topic(self,topic_name):
        return self.queue_obj.create_queue_for_topic(topic_name)
        
    def insert_into_topic_queue(self,topic_name,subscriber_id,message_id):
        return self.queue_obj.insert_into_topic_queue(topic_name,subscriber_id,message_id)

    def get_messages_from_queue(self,topic_name,client_id):
        return self.queue_obj.get_messages_from_queue(topic_name,client_id)