from Server.Entities.message import Message

class MessageService():

    def __init__(self,database_handler):
        self.database_handler = database_handler
        self.message_obj = Message(database_handler)

    def get_messages(self,topic_name,user_id):
        subscriber_id = user_id
        messages = self.message_obj.get_messages(topic_name,subscriber_id)
        self.message_obj.update_message_status(topic_name,subscriber_id)
        messages_to_send = ''
        if(messages):
            for message in messages:
                messages_to_send = messages_to_send + message[0]+'\n'
            return messages_to_send
        return False

    def save_messages(self,topic_id,message,publisher_id):
        return self.message_obj.save_messages(topic_id,message,publisher_id)
    
    def get_messages(self,topic_name,subscriber_id):
        return self.message_obj.get_messages(topic_name,subscriber_id)

    def update_message_status(self,topic_name,subscriber_id):
        return self.message_obj.update_message_status(topic_name,subscriber_id)