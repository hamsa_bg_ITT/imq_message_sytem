from Server.Entities.topic import Topic

class TopicService():

    def __init__(self,database_handler):
        self.database_handler = database_handler
        self.topic_obj = Topic(database_handler)

    def handle_topic_creation(self,topic_name,creater_id):
        if not self.get_topic_id(topic_name):
            self.insert_into_topics_table(topic_name,creater_id)
            response_to_send = 'Topic created successfully\n'
        else:
            response_to_send = 'Topic exists with that name\n'
        return response_to_send
       

    def handle_subscription(self,topic_name,subscriber_id):
        topic_id = self.get_topic_id(topic_name)
        if(topic_id):
            self.topic_obj.add_subscription(topic_id,subscriber_id)
            response_to_send = 'Subscribed successfully\n'
        else:
            response_to_send = "Topic doesn't exists\n"
        return response_to_send

    def get_topics_names(self):
        topics = self.get_topics()
        messages_to_send = ''
        if(topics):
            for topic in topics:
                messages_to_send = messages_to_send + topic[1]+'\n'
            return messages_to_send
        return False


    def get_topics(self):
        return self.topic_obj.get_topics()
    
    def get_subscribed_topics(self,subscriber_id):
        topics = self.topic_obj.get_subscribed_topics(subscriber_id)
        messages_to_send = ''
        if(topics):
            for topic in topics:
                messages_to_send = messages_to_send + topic[1]+'\n'
            return messages_to_send
        else:
            return topics
        return False
        

    def get_topic_id(self,topic_name):
        return self.topic_obj.get_topic_id(topic_name)
    
    def add_subscription(self,topic_id,client_id):
        return self.topic_obj.add_subscription(topic_id,client_id)
        
    def get_subcribers(self,topic_id):
        return self.topic_obj.get_subcribers(topic_id)

    def insert_into_topics_table(self,topic_name,creater_id):
       return self.topic_obj.insert_into_topics_table(topic_name,creater_id)
        