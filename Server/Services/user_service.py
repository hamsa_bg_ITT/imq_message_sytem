from Server.Entities.user import User
class UserService():

    def __init__(self,database_handler):
        self.user_obj = User(database_handler)
        self.database_handler = database_handler

    def validate_for_user(self,username,password):
        return self.user_obj.validate_for_user(username,password)
        
    def insert_into_clients_table(self,client_name,password,type):
       return self.user_obj.insert_into_clients_table(client_name,password,type)
        
    def get_user_id(self,client_name):
        return self.user_obj.get_user_id(client_name)