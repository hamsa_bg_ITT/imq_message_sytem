import sys
sys.path.append('../')
import _thread
from SharedLibraries.Socket.socket_handler import SocketHandler
from Utilities.Handlers.file_handler import FileHandler
from Server.client_thread_handler import ClientThreadHandler
from Utilities.Handlers.log_handler import LogHandler

logger_obj = LogHandler()

class Server(SocketHandler):
    thread_count = 0
    
    def __init__(self):
        super().__init__() 

    def accept_connection(self):
        while True:
            self.client, self.address = self.socket_accept()
            print('Connected to: ' + self.address[0] + ':' + str(self.address[1]))
            self.create_thread()
        self.close_socket()
        
    def create_thread(self):
        _thread.start_new_thread(self.threaded_client, (self.client,self.address))
        self.thread_count += 1
        print('Client Number: ' + str(self.thread_count))

    def threaded_client(self, connection, address):
        ClientThreadHandler(connection,address,self.thread_count)
        

if __name__ == '__main__':
    try:
        server = Server()
        server.create_socket()
        server.socket_bind()
        server.socket_listen()
        server.accept_connection()
    except:
        error = str("something went wrong, closing connections")
        print(error)
        logger_obj.log_error(error)
