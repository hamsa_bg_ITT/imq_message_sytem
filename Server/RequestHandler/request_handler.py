from Database.database_handler import DataBaseHandler
from SharedLibraries.Protocols.protocol_parser import Parser
from Utilities import constants
from Server.Services.user_service import UserService
from Server.Services.queue_service import QueueService
from Server.Services.topic_service import TopicService
from Server.Services.message_service import MessageService

class RequestHandler():
    def __init__(self,socket_handler,valid,client_name,database_handler):
        self.socket_handler = socket_handler
        self.database_handler = database_handler
        self.client_name = client_name
        self.create_entities_instances()
        self.handle_request(valid)
        
    def handle_request(self,valid):
        if(valid):
            self.socket_handler.send_response(valid,constants.LOGIN_REQUEST)
            if(int(valid) == 1 ):
                response_to_send = 'Authentication successfull\n Please select one of the option 1.Create Topic 2.Push Message'
            else:
                response_to_send = 'Authentication successfull\n Please select one of the option 1.Subscribe to Topic 2.Pull Messages'
            self.socket_handler.send_response(response_to_send,constants.LOGIN_REQUEST)
            response = self.socket_handler.recv_response()
            if(response['request_type']== constants.TOPIC_CREATE_REQUEST):
                self.handle_topic_creation(response['data'])
            if(response['request_type']== constants.PUSH_REQUEST):
                self.push_message()
            if(response['request_type']== constants.SUBSCRIBE_REQUEST):
                self.add_subscription()
            if(response['request_type']== constants.PULL_REQUEST):
                self.handle_pull_request()
        else:
            response_to_send = 3
            self.socket_handler.send_response(response_to_send,constants.LOGIN_REQUEST)
        self.socket_handler.close_socket()
        exit()
    
    def push_message(self):
        topics = self.topic_service_obj.get_topics_names()
        if(topics):
            self.socket_handler.send_response(topics,constants.PUSH_REQUEST)
            message = self.socket_handler.recv_response()['data']
            data = message.split(' ',2)
            if(len(data)>1):
                topic_name = data[0]
                message = data[1]
                topic_id =  self.topic_service_obj.get_topic_id(topic_name)
                if(topic_id):
                    publisher_id = self.user_service_obj.get_user_id(self.client_name)
                    message_id = self.message_service_obj.save_messages(topic_id,message,publisher_id)
                    subscribers = self.topic_service_obj.get_subcribers(topic_id)
                    self.queue_service_obj.push_messages(subscribers,topic_name,message_id,message)
                    response_to_send = 'Pushed message successfully'
                    status = constants.REQUEST_SUCCESS
                else:
                    response_to_send = 'Topic not found'
                    status = constants.REQUEST_FAILURE
            else:
                response_to_send = 'required parameters were not sent'
                status = constants.REQUEST_FAILURE
            self.socket_handler.send_response(response_to_send,constants.PUSH_REQUEST,status)
        else:
            response_to_send = 'No topics exists'
            status = constants.REQUEST_FAILURE

        self.socket_handler.send_response(response_to_send,constants.PUSH_REQUEST,status)

        
    def handle_topic_creation(self,topic_name):
        creater_id = self.user_service_obj.get_user_id(self.client_name)
        response_to_send  = self.topic_service_obj.handle_topic_creation(topic_name,creater_id)
        self.queue_service_obj.create_queue_for_topic(topic_name)
        self.socket_handler.send_response(response_to_send,constants.TOPIC_CREATE_REQUEST)
        
    def add_subscription(self):
        topics = self.topic_service_obj.get_topics_names()
        if(topics):
            self.socket_handler.send_response(topics,constants.PUSH_REQUEST)
            topic_name = self.socket_handler.recv_response()['data']
            subscriber_id = self.user_service_obj.get_user_id(self.client_name)
            response_to_send = self.topic_service_obj.handle_subscription(topic_name ,subscriber_id)
            status = constants.REQUEST_SUCCESS
        else:
            response_to_send = 'No topics available'
            status = constants.REQUEST_FAILURE
        self.socket_handler.send_response(response_to_send,constants.PUSH_REQUEST,status)
        
    def get_messages(self,topic_name):
        subscriber_id = self.user_service_obj.get_user_id(self.client_name)
        return self.message_service_obj.get_messages(topic_name,subscriber_id)
    
    
    def handle_pull_request(self):
        subscriber_id = self.user_service_obj.get_user_id(self.client_name)
        topics = self.topic_service_obj.get_subscribed_topics(subscriber_id)
        if(topics):
            self.socket_handler.send_response(topics,constants.PULL_REQUEST)
            topic_name = self.socket_handler.recv_response()['data']
            if(self.topic_service_obj.get_topic_id(topic_name)):
                # response_to_send = self.get_messages(topic_name)
                response_to_send = self.queue_service_obj.get_messages_from_queue(topic_name,subscriber_id)
                if not (response_to_send):
                    response_to_send = 'No Messages, pulled successfully'
            else:
                response_to_send = "Topic doesn't exists"
            status = constants.REQUEST_SUCCESS
        else:
            response_to_send = "You haven't subscribed to any"
            status = constants.REQUEST_FAILURE
        self.socket_handler.send_response(response_to_send,constants.PULL_REQUEST,status)

    def create_entities_instances(self):
        self.user_service_obj = UserService(self.database_handler)
        self.topic_service_obj = TopicService(self.database_handler)
        self.message_service_obj = MessageService(self.database_handler)
        self.queue_service_obj = QueueService(self.database_handler)
        