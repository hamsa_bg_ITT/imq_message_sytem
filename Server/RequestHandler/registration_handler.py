from Utilities import constants
from Server.Services.user_service import UserService

class RegistrationHandler():
    def __init__(self,socket_handler,database_handler):
        self.database_handler = database_handler
        self.user_service_obj = UserService(database_handler)
        response_to_send = 'For registration please enter username, password, and role(1 for publisher and 2 for subscriber) separated by space'
        socket_handler.send_response(response_to_send,constants.REGISTRATIONS_REQUEST)
        response = socket_handler.recv_response()
        credentials = response['data'].split()
        if(len(credentials)==3):
            self.client_name = credentials[0]
            if not self.user_service_obj.get_user_id(self.client_name):
                password = credentials[1]
                role = credentials[2]
                self.register_with_database(password,role)
                response_to_send = 'Registration successfull'
            else:
                response_to_send = "User exists with the given name"
            socket_handler.send_response(response_to_send,constants.REGISTRATIONS_REQUEST)
        else:
            response_to_send = 'Registration unsuccessfull because required credentials were not found'
            socket_handler.send_response(response_to_send,constants.REGISTRATIONS_REQUEST)
            
    def register_with_database(self,password,role):
        self.user_service_obj.insert_into_clients_table(self.client_name,password,role)