from Utilities import constants
from Server.RequestHandler.request_handler import RequestHandler
from Server.Services.user_service import UserService

class LoginHandler():
    
    def __init__(self,socket_handler,database_handler):
        self.database_handler = database_handler
        self.socket_handler = socket_handler
        self.user_service_obj = UserService(database_handler)
        self.handle_login()
        
    def validate_for_user(self,username,password):
        return self.user_service_obj.validate_for_user(username,password)
    
    def handle_login(self):
        response_to_send = 'For login please enter username, password separated by space\n'
        self.socket_handler.send_response(response_to_send,constants.LOGIN_REQUEST)
        response = self.socket_handler.recv_response()
        credentials = response['data'].split()
        if(len(credentials)==2):
            self.client_name = credentials[0]
            password = credentials[1]
            valid = self.validate_for_user(self.client_name,password)
            RequestHandler(self.socket_handler,valid,self.client_name,self.database_handler)
        else:
            response_to_send = 'Authentication unsuccessfull because required parameters not recieved'
            self.socket_handler.send_response(response_to_send,constants.LOGIN_REQUEST)
            self.socket_handler.close_socket()
            exit()
        
    