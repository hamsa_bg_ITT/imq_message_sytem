import sys
sys.path.append('../')

from Database.database_handler import DataBaseHandler
from Server.RequestHandler.registration_handler import RegistrationHandler
from Server.RequestHandler.login_handler import LoginHandler
from Utilities import constants
from SharedLibraries.Socket.socket_handler import SocketHandler

class ClientThreadHandler():
    
    def __init__(self,socket,address,client_id):
        self.database_handler = DataBaseHandler()
        self.socket_handler = SocketHandler()
        self.socket_handler.set_socket(socket)
        self.client_id = client_id
        self.handle_connection()
        
        
    def handle_connection(self):
        while True:
            response_to_send = 'Welcome to the Server'
            self.socket_handler.send_response(response_to_send,constants.NO_REQUEST)
            data =  self.socket_handler.recv_response()
            if not data:
                break
            if(data['request_type'] == constants.REGISTRATIONS_REQUEST):
                self.handle_registration()
            if(data['request_type'] == constants.LOGIN_REQUEST):
                self.handle_login()
        print("closing connection of client "+str(self.client_id))
        self.socket_handler.close_socket()
        exit()
    
    def handle_registration(self):
        RegistrationHandler(self.socket_handler,self.database_handler)
            
    def handle_login(self):
        LoginHandler(self.socket_handler,self.database_handler)
    