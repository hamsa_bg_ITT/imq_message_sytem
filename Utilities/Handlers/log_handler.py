import logging
import datetime
import time

log_filename = '../Logs/app.log'
class LogHandler:
    
    def __init__(self):
        logging.basicConfig(filename=log_filename, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
    
    def log_error(self,error):
        error = self.get_time() + error
        logging.error(error)
    
    def log_warning(self,warning):
        warning = self.get_time()+ warning
        logging.warning(warning)
        
    def get_time(self):
        timestamp = time.time()
        connection_time = datetime.datetime.fromtimestamp(
            timestamp).strftime('%Y-%m-%d::%H:%M:%S - ')
        return connection_time
        