import os.path
from Utilities.datetime import Datetime
from SharedLibraries.ExceptionHandling.file_handler_exception import FileHandlerException
import time

datetime_obj = Datetime()

class FileHandler:
        
    def write_into_file(self,directory,client_name,data):
        try:
            timestamp = time.time()
            connection_time = datetime_obj.get_time_stamp()
            filename = directory+"/"+'{0}.txt'.format(client_name)
            with open(filename, "a") as file:
                    file.write(connection_time + data + '\n')
        except FileHandlerException:
            print("Error occured while writing the file")
    
    def create_directory(self,directory_name):
        try:
            if not os.path.exists(directory_name):
                os.makedirs(directory_name)
        except FileHandlerException:
            print("Error occured whilecreating the directory")
        
