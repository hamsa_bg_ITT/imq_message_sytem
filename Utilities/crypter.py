class Crypter: 

    def encode_data(data):
        return str.encode(data)
        
    def decode_data(data):
        return data.decode('utf-8')
