import mysql.connector
from Utilities import constants
from Utilities.Handlers.log_handler import LogHandler
from SharedLibraries.ExceptionHandling.database_exception import DatabaseException
logger_obj = LogHandler()


class Initializer:
    
    
    def __init__(self,database_name =constants.DATEBASE_NAME ):
        self.connect()
        self.disconnect()
        self.database_name = database_name

    def connect(self):
        self.mydb = mysql.connector.connect(
        host="localhost",
        user=constants.DATABASE_USER,
        password=constants.DATABASE_PASSWORD,autocommit=False
        )
        self.mycursor = self.mydb.cursor()
        
    def execute_query(self,query):
        self.connection_establishment()
        try:
            if(self.mydb.in_transaction):
                self.mydb.rollback()
            self.mydb.start_transaction()
            self.mycursor.execute(query)
            self.mydb.commit()
        except DatabaseException:
            self.mydb.rollback()
            self.show_error()
        self.disconnect()
            
    def fetchone_query(self,query):
        self.connection_establishment()       
        try:
            self.mycursor.execute(query)
            result = self.mycursor.fetchone()
            if result:
                return result[0]
            return False
        except DatabaseException:
            self.show_error()
        self.disconnect()
            
    def fetch_query(self,query):
        self.connection_establishment()
        try:
            self.mycursor.execute(query)
            result = self.mycursor.fetchmany(100)
            if result:
                return result
            return False
        except DatabaseException:
            self.show_error()
        self.disconnect()
    
    def connection_establishment(self):
        try:
            self.mydb = mysql.connector.connect(
            host="localhost",
            user=constants.DATABASE_USER,
            password=constants.DATABASE_PASSWORD,
            database=self.database_name
            )
            self.mycursor = self.mydb.cursor()
        except DatabaseException:
            self.show_error()
        
    def disconnect(self):
        try:
            self.mycursor.close()
        except DatabaseException:
            self.show_error()

    def show_error(self):
        error = "Something went wrong while executing the query: {}".format(mysql.connector.Error)
        print(error)
        logger_obj.log_error(error)
        exit()

        