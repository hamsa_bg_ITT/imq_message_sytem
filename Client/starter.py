import sys
sys.path.append('../')
from SharedLibraries.Socket.socket_handler import SocketHandler
from Utilities import constants
from Client.Types.publisher import Publisher
from Client.Types.subscriber import Subscriber

class Client(SocketHandler):
    
    def __init__(self):
        super().__init__() 

    def read_choice(self):
        request = input("Please select one of the following choices \n1.Registration \n2.Login\n")
        if int(request) == constants.REGISTRATIONS_REQUEST:
            self.send_response(request,constants.REGISTRATIONS_REQUEST)
            self.handle_registration()
        elif int(request) == constants.LOGIN_REQUEST:
            self.send_response(request,constants.LOGIN_REQUEST)
            self.handle_login()
    
    def read_response(self):
        response = self.recv_response()
        print('Response from Server: '+response['data']+'\n')
        self.read_choice()
        self.close_socket()
        
    def handle_login(self):
        response = self.recv_response()
        credentials = input(response['data'])
        self.send_response(credentials,constants.LOGIN_REQUEST)
        response = self.recv_response()
        role = response['data']
        if(len(str(role))>1 and role):
            print(role)
        else:
            if(int(role)==1):
                Publisher(self.socket_obj)
            elif(int(role)==2):
                Subscriber(self.socket_obj)
            elif(int(role)==3):
                print('invalid credentials\n')
                
    def handle_registration(self):
        response = self.recv_response()
        credentials = input('Response from Server: '+response['data']+'\n')
        self.send_response(credentials,constants.REGISTRATIONS_REQUEST)
        response = self.recv_response()
        print('Response from Server: '+response['data']+"\n")
        
if __name__ == '__main__':
    client = Client()
    client.create_socket()
    client.connect_socket()
    client.read_response()
