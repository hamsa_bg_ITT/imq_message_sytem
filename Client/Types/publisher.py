import sys
sys.path.append('../')
from Utilities import constants
from SharedLibraries.Socket.socket_handler import SocketHandler

class Publisher():
    
    def __init__(self,socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        print("Publisher\n")
        response = self.socket_handler.recv_response()
        choice = input(response['data']+"\n")
        if(choice=="1"):
            self.create_topic()
        else:
            self.push_message()
    
    def create_topic(self):
        topic_name = input("Please enter topic name\n")
        self.socket_handler.send_response(topic_name,constants.TOPIC_CREATE_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data']+"\n")
    
    def push_message(self):
        self.socket_handler.send_response(None,constants.PUSH_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data']+"\n")
        if(response['status']):
            data = input("Please enter topic name and push message\n")
            self.socket_handler.send_response(data,constants.PUSH_REQUEST)
            response = self.socket_handler.recv_response()
            print(response['data']+"\n")
        
        