
from Utilities import constants
from SharedLibraries.Socket.socket_handler import SocketHandler

class Subscriber:        
        
    def __init__(self,socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        print("Subscriber\n")
        response = self.socket_handler.recv_response()
        choice = input(response['data']+"\n")
        if(choice=="1"):
            self.subscribe_to_topic()
        else:
            self.pull_message()
    
    def subscribe_to_topic(self):
        self.socket_handler.send_response(None,constants.SUBSCRIBE_REQUEST)
        response = self.socket_handler.recv_response()
        print(response['data']+"\n")
        if(response['status']):
            topic_name = input("Please enter topic name\n")
            self.socket_handler.send_response(topic_name,constants.SUBSCRIBE_REQUEST)
            response = self.socket_handler.recv_response()
            print(response['data']+"\n")
    
    def pull_message(self):
        self.socket_handler.send_response(None,constants.PULL_REQUEST)
        response = self.socket_handler.recv_response()
        if(response['status']):
            print(response['data']+"\n")
            topic_name = input("Please enter topic name to pull messages\n")
            self.socket_handler.send_response(topic_name,constants.PULL_REQUEST)
            response = self.socket_handler.recv_response()
        print(response['data']+"\n")