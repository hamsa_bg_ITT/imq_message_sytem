import sys
sys.path.append('../')
import unittest
from Server.starter import Server

class Test(unittest.TestCase):
    server_obj = Server()

    def test_for_socket_creation(self):
        self.assertTrue((type(self.server_obj.create_socket())))
        self.server_obj.close_socket()

    def test_for_socket_bind(self):
        self.server_obj.create_socket()
        self.assertTrue((type(self.server_obj.socket_bind())))
        self.server_obj.close_socket()
    
    def test_for_socket_listening(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.assertTrue((type(self.server_obj.socket_listen())))
        self.server_obj.close_socket()
        
        
if __name__ == '__main__':
    unittest.main()