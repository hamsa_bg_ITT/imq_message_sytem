import sys
sys.path.append('../')
import unittest
from Server.starter import Server
from Client.starter import Client

class Test(unittest.TestCase):
    client_obj = Client()
    server_obj = Server()

    def test_for_socket_creation(self):
        self.assertTrue((type(self.client_obj.create_socket())))
        self.assertTrue((type(self.server_obj.create_socket())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()

    def test_for_socket_bind(self):
        self.server_obj.create_socket()
        self.assertTrue((type(self.server_obj.socket_bind())))
        self.server_obj.close_socket()
    
    def test_for_socket_listening(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.assertTrue((type(self.server_obj.socket_listen())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()

    def test_for_socket_connection_establishment(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.server_obj.socket_listen()
        self.client_obj.create_socket()
        self.assertTrue((type(self.client_obj.connect_socket())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()
        
    
    def test_for_socket_request_acception(self):
        self.server_obj.create_socket()
        self.server_obj.socket_bind()
        self.server_obj.socket_listen()
        self.client_obj.create_socket()
        self.client_obj.connect_socket()
        self.assertTrue((type(self.server_obj.socket_accept())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()
        
        
if __name__ == '__main__':
    unittest.main()