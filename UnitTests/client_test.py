import sys
sys.path.append('../')
import unittest
from Client.starter import Client

class Test(unittest.TestCase):
    client_obj = Client()

    def test_for_socket_creation(self):
        self.assertTrue((type(self.client_obj.create_socket())))
        self.client_obj.close_socket()
        
        
if __name__ == '__main__':
    unittest.main()