import sys
sys.path.append('../')
import unittest
from SharedLibraries.Protocols.protocol_parser import Parser
from SharedLibraries.Protocols.request import Request

class Test(unittest.TestCase):
    request = Request("message",0)

    def test_for_json_encode(self):
        message = "message need to be encoded"
        expected_result  = '"\\"message need to be encoded\\""'
        actual_result = Parser.get_json_encoded(message)
        self.assertEqual(expected_result,actual_result)

    def test_for_json_decode(self):
        expected_result = "message need to be encoded"
        message  = '"\\"message need to be encoded\\""'
        actual_result = Parser.get_json_decoded(message)
        self.assertEqual(expected_result,actual_result)

    def test_for_request_json_encoded(self):
        expected_result ='"{\\"data\\": \\"message\\", \\"request_type\\": 0, \\"status\\": 1, \\"version\\": 5, \\"format\\": \\"json\\", \\"requestType\\": \\"connect_to_topic\\"}"'
        actual_result = Parser.get_json_encoded(self.request) 
        self.assertEqual(expected_result,actual_result)

if __name__ == '__main__':
    unittest.main()