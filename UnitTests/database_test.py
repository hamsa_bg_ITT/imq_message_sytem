import sys
sys.path.append('../')
import unittest
from Database.init import Initializer

class Test(unittest.TestCase):
    database_obj = Initializer("test")

    def test_for_connection(self):
        self.assertTrue((type(self.database_obj.connect())))

    def test_for_drop_database_exists(self):
        query = "DROP DATABASE IF EXISTS test"
        self.assertTrue((type(self.database_obj.execute_query(query))))

    def test_for_database_creation(self):
        query = "CREATE DATABASE IF NOT EXISTS test"
        self.assertTrue((type(self.database_obj.mycursor.execute(query))))

    def test_for_database_change(self):
        query = "USE test"
        self.assertTrue((type(self.database_obj.mycursor.execute(query))))
        
if __name__ == '__main__':
    unittest.main()